#!/usr/bin/perl

use 5.006;
use strict;
use warnings;

sub crearArchivos {
	my $dir = shift;
	print "Creando directorio en '$dir'\n";
	opendir(DIR, $dir ) || die "No puedo abrir el directorio $dir";
	open(ARCHIVO, "> $dir/__archivos__") 
		|| die "No puedo crear el directorio $dir/__archivos__";

	my @visitar;
	
	for( readdir(DIR) ) {
		next  if /__archivos__/;
		my $d = $dir . $_;
		push @visitar, $_
			if ( -d $d && $_ !~ /\./);
		print ARCHIVO "$_\n";
	}

	close ARCHIVO;
	closedir DIR;
	
	for(@visitar) {
		crearArchivos("$dir$_/");
	}
}


my $directorio = shift || die "No se especifica directorio";
crearArchivos($directorio);

