
tYO=""

if [[ "$0" == "bash" ]]; then
    ### llamado en el contexto actual
    tYO=$BASH_ARGV
else
    tYO=$0
fi

tGIT=$(which git)
tPERL=$(which perl)
tMAKE=$(which make)
tSVN=$(which svn)
tRAKUDO_PADRE=$(pwd)/$(dirname $tYO)/../..
tRAKUDO_HOME=$tRAKUDO_PADRE/rakudo
tPRG=$(basename $tYO)


mostrar() {
    echo -e "\n$tPRG: $1\n"
}

buscarSVN() {
    if [[ "$tSVN" == "" ]]; then
        msgThis "No se encuentra el cliente de Subversion (svn)"
        msgThis "Por favor, agréguelo al PATH"
        exit -1
    fi
}

buscarGit() {
    if [[ "$tGIT" == "" ]]; then
        msgThis "No se encuentra el cliente de Git (git)"
        msgThis "Por favor, agréguelo al PATH"
        exit -1
    fi
}

buscarPerl() {
    if [[ "$tPERL" == "" ]]; then
        msgThis "No se encuentra el programa perl"
        msgThis "Por favor, agréguelo al PATH"
        exit -1
    fi
}

buscarMake() {
    if [[ "$tMAKE" == "" ]]; then
        msgThis "No se encuentra el programa make"
        msgThis "Por favor, agréguelo al PATH"
        exit -1
    fi
}

todoBienO() {
    if [[ $? -ne 0 ]]; then
        mostrar "$1"
        exit -1
    fi
}

instalarRakudo() {
    mostrar "Instalando Rakudo"
    $tMAKE install
    todoBienO "Falló la instalación de Rakudo"
}
