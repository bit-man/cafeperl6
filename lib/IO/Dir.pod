
=pod

La clase IO::Directory implementa las funciones de manejo de directorio (opendir, etc.) que 
parecen no estar implementadas en Rakudo (Diciembre 2009). Para verificar si la implementación
está disponible en Rakudo se utiliza el archivo t/spectest.data del repositorio de Rakudo y 
debe figurar que pasa los tests pertenecientes a S16-filehandles/dir.t

Esta es una implementación básica sólo a los efectos de poder ser usada internamente
por los distintos programas del repositorio de Perl 6 de CaFe PM. Se toleran ciertas desviaciones
respecto de la interfaz propuesta para Perl 6

=head1  Detalles de implementación

=over 4

=item * Se implementa como una clase y no como un rol solamente por simplicidad (tampoco
parece estar definido el rol IO::Streamable).

=item * Como no se hayan implementadas las primitivas para leer directorios se reempleza este
por un mecanismo en el cual las entradas de un directorio se encuentra pre-listadas en un
archivo llamado __archivos__, a razón de uno por línea, equivalente a ejecutar el siguiente
comando en un shell :

    ls -la | awk '{print $8}' 

o en perl 5

	opendir(DIR, $directorio ) || die;
	open(ARCHIVO, "> $directorio"."__archivos__");
	for( readdir(DIR) ) {
		next  if /__archivos__/;
		print ARCHIVO "$_\n";
	}
	close ARCHIVO;
	closedir DIR;

=item * No se implementaron todas las posibles interfaces de los métodos que requieren el 
uso de polimorfismo basado en el valor devuelto (ver Polimorfismo). Los métodos 
implementados son read y readdir sólo cuando es requerida la devolución de un array.

=item * Cómo hay dos formas de llamar a opendir y sus asociadas (como primitivas internas
de Rakudo o como parte de la clase IO::Dir) se generó la clase IO::Dir que tiene la
implementación de cada método más un módulo llamado Opendir que implementa las primitivas y
llama a los métodos de la clase IO::Dir que son quiénes tienen la implementación final.

=back
 
=head1 Adecuaciones realizadas

Aún no se encuentra realizado el módulo FindBin para Perl6 que se necesita para ejecutar 
el test S16-filehandles/dir.t, con lo cual se usará el mismo test, pero modificado, para
que no use este módulo. La forma de ejecutarlo es:

               perl6 t/S16-filehandles/dir.t

=over 4

=item * No se usa el módulo FindBin. Este sirve para obtener un directorio fijo y no requerir de
parámetros. Se reemplazó por el scalar $testPath

=item * Se agrega el uso del módulo Opendir, donde se implementan las funciones que son llamadas como
sin usar el nommbre de la clase (por ej. opendir en lugar de IO::Dir.opendir)

=back


=head1 Agregado de operaciones como primitivas de Rakudo

Para agregar las operaciones de la familia opendir deben ejecutarse los siguientes pasos 
(todas las referencias a los archivos son relativas al directorio root de Rakudo)

=over 4

=item * generar el archivo src/builtins/dir.pir

=item * agregar al Makefile el archivo src/builtins/dir.pir a la variable BUILTINS_PIR

=item * agregar la sentencia ".include 'src/builtins/dir.pir'" en el archivo src/gen_builtins.pir

=back


=head2 Polimorfismo

Aún no entiendo si es que Perl 6 no usa el tipo del valor devuelto como parte de la forma 
del método o simplemente aún no está implementado. Tomemos el siguiente ejemplo :

    class miClase {
    	
    	. . .
    	
		multi method pp($self: --> Str) {
			say "Str";
			my Str $a;
			return $a;
		}
    
		multi method pp($self: --> Int) {
			say "Int";
			my Int $a;
			return $a;
		}
		
		. . .
    }
    
    my $a = miClase.new;
	my Int $x = $a.pp();
	my Str $y = $a.pp();

Va a llamar siempre a la primer implementación definida, y en este caso lo que vamos a
ver por stdout es :

	Str
	Str

mostrando que siempre es llamada la primer implementación sin importar el tipo de valor
devuelto.

}

=head1 Pendientes

=over 4

=item * Completar la implementación de dir.pir

=item * Hacer el módulo FindBin para Perl6 (adaptarlo del original)

=item * Usar el nuevo formato POD para Perl 6. En principio se puede usar =begin pod y 
=end pod como delimitadores de documentación para que el código de Perl6 pueda compilar

=back


=head1 Hallazgos

Si bien hay una buena parote implementada de Rakudo no veo factible que se pueda hacer opendir 
sólo usando Perl6, no veo que las primitivias estén hechas (de hecho al ser opendir parte de
las primitivas sería lógico que no pueda basarse en primitivas ya existentes)

Opciones:

=over 4

=item * implementación en Perl 6 : usando las primitivas existentes en Rakudo realizar una
implementación pura en Perl 6, realizando las adecuaciones pertinentes

=item * implementación usando PIR : necesito aprender el lenguaje, lo que no me atrae mucho

=item * implementación con un mínimo en PIR y el resto en Perl6, lo que veo más viable dado que no 
sólo no me atrae mucho tener que usar PIR sino que parece más elegante poder usar el 
propio lenguaje y minimizar el uso de PIR (además que no tengo buen acceso de Internet
durante las vacaciones :-P)

=back

=head1 Documentación

=over 4

=item * IO::Directory Synopsys : http://perlcabal.org/syn/S32/IO.html#IO%3A%3ADirectory

=item * Repositorio de Perl 6 de CaFe PM : http://code.google.com/p/cafeperl6/ 

=item * Clases y roldes de IO : http://perlcabal.org/syn/S16.html#Roles_and_Classes

=item * Funciones internas, IO : http://perlcabal.org/syn/S29.html#IO

=item * Test de funciones de directorios (S16-filehandles/dir.t)

=back

=cut