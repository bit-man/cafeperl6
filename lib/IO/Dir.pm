class IO::Dir;

has $!pathDirectorio is rw;
has $!dh is rw;

## $archivo termina con un separador de nombre de directorio (ej. /)

method new ($self: Str $directorio --> IO::Dir) {
	$self.bless(*, pathDirectorio => $directorio,
			dh => open $directorio ~ '__archivos__', :r  )
}


multi method  readdir($self: --> Array) {
	my @respuesta;
	while ! $!dh.eof {
            @respuesta.push( $!dh.get );
        }
	return @respuesta;
}

## Debería devolver un Scalar, pero todavía no está definido en Rakudo
multi method readdir($self: --> Str) {
	if ( ! $!dh.eof ) {
		return $!dh.get;
	} else {
		return undef;
	}
}

multi method read ($self: --> Array) {
	return $self.readdir;
}

method rewinddir {
	## seek() no está implementado todavía
	$!dh.close();
	$!dh = open $!pathDirectorio ~ '__archivos__', :r;
	return 1;
}

method rewind($self:) {
	return $self.rewinddir;
}

method closedir {
	return $!dh.close();
}

method close($self:) {
	return $self.closedir;
}