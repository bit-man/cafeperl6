module Opendir;

use  IO::Dir;

sub opendir($pathDirectorio) is export(:DEFAULT) {
	return IO::Dir.new($pathDirectorio);
}
## Parece ser que para las sub no se definen con --> el tipo de datos devuelto
## - se hace como en Perl 5 con wantarray ?
## - se especifica ed otra forma ?
## - no se especifica en lo absoluto ??

multi sub readdir(Object $dh) is export(:DEFAULT) {
		my @ret = $dh.readdir;
		return @ret;
}

sub rewinddir(Object $dh) is export(:DEFAULT) {
	return $dh.rewinddir;
}

sub closedir(Object $dh) is export(:DEFAULT) {
	return $dh.closedir;
}

sub read(Object $dh) is export(:DEFAULT) {
	return $dh.readdir;
}

sub rewind(Object $dh) is export(:DEFAULT) {
	return $dh.rewind;
}

sub close(Object $dh) is export(:DEFAULT) {
	return $dh.close;
}
