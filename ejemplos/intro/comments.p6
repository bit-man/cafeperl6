#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

say "Hello";

# This is a single line comment

say #`< embedded comments > "hello world";

 #`[
   This is a 
   multiline 
   comment
   It starts by a # followed by an opening bracket
   and end with the bracket pair
  ]


 #`<<<
   but it has a bug now
   that it cannot start on the first character of a line
 >>>
