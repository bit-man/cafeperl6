#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

print "First number:";
my $a = $*IN.get;
print "Second number:";
my $b = $*IN.get;

my $c = $a / $b;

say "\nResult: $c";

