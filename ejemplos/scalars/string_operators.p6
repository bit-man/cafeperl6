#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $x = "Hello";
my $y = "World";

# ~ is the concatenation operator, ataching ons string after the other
my $z = $x ~ " " ~ $y;  #       the same as "$x $y"
say $z;                 # Hello World



my $w = "Take " ~ (2 + 3);
say $w;                        # Take 5
say "Take 2 + 3";              # Take 2 + 3 
say "Take {2 + 3}";            # Take 5



$z ~= "! ";             #       the same as  $z = $z ~ "! ";
say "'$z'";             # 'Hello World! '
