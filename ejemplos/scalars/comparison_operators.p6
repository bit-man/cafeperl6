#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

say 4      == 4 ?? "TRUE" !! "FALSE";     # TRUE
say 3      == 4 ?? "TRUE" !! "FALSE";     # FALSE
say "3.0"  == 3 ?? "TRUE" !! "FALSE";     # TRUE
say "3.0"  eq 3 ?? "TRUE" !! "FALSE";     # FALSE
say 13     >  2 ?? "TRUE" !! "FALSE";     # TRUE
say 13     gt 2 ?? "TRUE" !! "FALSE";     # FALSE
say "foo"  == "" ?? "TRUE" !! "FALSE";    # TRUE
say "foo"  eq "" ?? "TRUE" !! "FALSE";    # FALSE
say "foo"  == "bar" ?? "TRUE" !! "FALSE"; # TRUE
say "foo"  eq "bar" ?? "TRUE" !! "FALSE"; # FALSE

