#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $z = "Hello World! ";

# x is the string repetition operator
my $q = $z x 3;
say "'$q'";         # 'Hello World! Hello World! Hello World! '

