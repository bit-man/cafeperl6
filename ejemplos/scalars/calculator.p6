#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $a        = prompt "Number:";
my $operator = prompt "Operator: [+-*/]:";
my $b        = prompt "Number:";

if $operator eq "+" {
    say $a + $b;
} elsif $operator eq "-" {
    say $a - $b;
} elsif $operator eq "*" {
    say $a * $b;
} elsif $operator eq "/" {
    say $a / $b;
} else {
    say "Invalid operator $operator";
}
