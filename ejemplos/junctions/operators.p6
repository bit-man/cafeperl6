#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $options = 1|2|3;

$options *= 2;    # now it has 2|4|6

for 1..8 -> $i {
    if ($i == $options) {
        say $i;
    }
}

