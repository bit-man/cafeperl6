#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $possible_options = 1|2|3;
print "please give a number(1,2 or 3): ";
my $c = $*IN.get;
if ($c == $possible_options) {
    say "Correct choice: $c";
} else {
    say "Incorrect choice: $c";
}

