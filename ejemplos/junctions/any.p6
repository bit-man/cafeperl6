#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my @names = <Foo Bar Moo>;
print "Please type in your name: ";
my $username = $*IN.get;
if $username eq any(@names) {
    say "Welcome $username";
} else {
    say "Unknown user $username";
}



#if ($username eq any(@names)) {
#    say "Welcome $username";
#}

