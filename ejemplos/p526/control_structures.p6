#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

# Perl5: if
# No need to for () around the condition
my $x = 23;
if $x < 42 {
}


# Perl5: foreach (for)
my @names = <Larry Guido Matz>;
for @names -> $person {
    say $person;
}

# Perl5: for (foreach)
loop (my $i=1; $i < 10; $i++) {
    say $i;
}

# infinite loop:
loop {
    # well, almost infinite :-)
    last;
}


# Perl5: while
while $i < 10 {
    say $i;
}


