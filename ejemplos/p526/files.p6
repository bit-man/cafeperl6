#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;


# Perl5: open
my $fh = open 'ejemplos/p526/files.p6', :r;

# Perl5: close
close($fh);
$fh.close


# Perl5: <$fh>
# $fh.readline


