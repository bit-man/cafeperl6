#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

# Perl5: my
my $name = "Moose";         # you always need "my"
say "Hello $name";          # still interpolates

# Perl5: <STDIN>
my $line = $*IN.get;           # reading from standard input, chomps automatically

# Perl5: substr
say substr $line, 2, 3;     # substr works as before (also the 4 paramter version)
say $line.substr(2, 3);     # also works this way

# Perl5: length
my $a = "This is a string";
say chars $a;               # the number of characters (was length)
say $a.chars;               # also works

#say bytes $a;               # the number of bytes
#say $a.bytes;               # also works

# Perl5: chomp
my $b = chomp $a;           # returns the chomped string, does NOT ! change $a;
my $c = $a.chomp;           # also works

# Perl5: defined
defined $b;                 # check if $b is defined or "undef"
$b.defined;                 # also works

# Perl5: undef
$b = undef;                 # set $b to "undef"

# Perl5: qq
# Perl5: q

# Perl5: local
# temp

# Perl5: $$, $@, $!, ...
# Perl5: $_
#
# Perl5: $0
# $?FILE

# Perl5: %ENV
# %*ENV




