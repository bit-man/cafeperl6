#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

# Perl5: print
print "Hello World\n";  # still works
say "Hello World";      # the same as print but adds a \n automatically

