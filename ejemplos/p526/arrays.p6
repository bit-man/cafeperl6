#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my @perl = ("one", "two", "three"); # creating an array
# Perl5: qw
my @perl6 = <one two three>;        # ceating an array


say @perl;                          # onetwothree       just as in the old days
say "@perl";                        # @a                it does not interpolate any more
say "joe@perl.org";                 # joe@perl.org      so this works now

# Perl5: array interpolation
say "{@perl}";                      # one two three     is the way to interpoalte an array 

# Perl5: array element
say @perl[0];                       # one               when accessing the elements of an array we use the @ sigil

# Perl5: number of elements in array
say elems(@perl);                   # 3                 number of elements
say @perl.elems;                    # also works


