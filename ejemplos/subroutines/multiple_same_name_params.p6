#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

# TODO named parameters
f(1, 4);           # 1  4
#f(y => 6, x => 2); # 2  6
#f(2, y => 6);      # 2  6
#f(2, x => 6);      # 6  2

sub f($x, $y) {
    say "$x $y";
}

sub u($x, $y?) {
}

sub x($x, $y = 7) {
}



