#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

# search the text within a file return 1 if found, 0 if not
sub search ($text, $file) {
	my $fh = open $file, :r orelse die;
	for $fh.readline -> $line {
		if index($line, $text) > -1 {
			return 1;
		}
	}
	return 0;
}

# optional parameter
sub search($text, $file, $all?) {
	my $fh = open $file, :r orelse die;
	my $cnt = 0;
	for $fh.readline -> $line {
		if index($line, $text) > -1 {
			return 1 if not $all;
			$cnt++;
		}
	}
	return $cnt;
}

