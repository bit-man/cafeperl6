#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

sub add ($a, $b) {
    return $a + $b;
}

say add(2, 3);      # returns 5

#say add(2);        # is an error
#say add(2, 3, 4);  # is an error
