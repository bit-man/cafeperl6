#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my @colors = <Blue Yellow Brown White>;

for 1..@colors.elems -> $i {
    say "$i) {@colors[$i-1]}";
}

print "Please select a number: ";
my $input = $*IN.get;
say "You selected {@colors[$input-1]}";

