#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $file = 'ejemplos/arrays/process_csv_file.csv';
if (defined @*ARGS[0]) {
    $file = @*ARGS[0];
}
   
my $sum = 0;
my $data = open($file, :r);
for $data.lines -> $line {
    my @columns = split ",", $line;
    $sum += @columns[2];
}
say $sum;

