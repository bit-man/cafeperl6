#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $color = @*ARGS[0];


if (not $color) {
    my @colors = ("Blue", "Yellow", "Brown", "White");

    for 1..@colors.elems -> $i {
        say "$i) {@colors[$i-1]}";
    }

    print "Please select a number: ";
    my $input = =$*IN;
    $color = @colors[$input-1];
}

say "You selected $color";

