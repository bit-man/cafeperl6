#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my @a = (1, 2, 3);
my @b = (7, 10, 12);

my @c = @a »+« @b;
say join ",", @c;


#TODO
#my @d = @c >>+<< 3;  ##???
#say "{@d}";

# my @e = @c >>+>> 3;  ##???

#my @x = (1, 2, 3) >>+<< (4, 5);  # 5 7 3
#say "{@x}"; 

