#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

say "Type in two values:";
my $a = $*IN.get;
my $b = $*IN.get;

($a, $b) = ($b, $a);
say $a;
say $b;

