#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $owner = "Moose";
my @tenants = "Foo", "Bar";
my @people = ($owner, 'Baz', @tenants);  # the array is flattened:
say "{@people}";                         # Moose Baz Foo Bar


my ($x, @y)     = (1, 2, 3, 4); 
say $x;                              # $x = 1
say "{@y}";                          # @y = (2, 3, 4)

