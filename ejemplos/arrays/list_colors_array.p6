#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my @colors = ("Blue", "Yellow", "Brown", "White");
say @colors;

say "--------------------------------";              # just for separation...

say "{@colors}";                                     

say "--------------------------------";              # just for separation...

for @colors -> $color {
    say $color;
}

