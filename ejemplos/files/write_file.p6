#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $filename = "temp.txt";

if (my $fh = open $filename, :w) {
    $fh.print("Your name: ");
    my $name = $*IN.get;
    $fh.say($name);
}

