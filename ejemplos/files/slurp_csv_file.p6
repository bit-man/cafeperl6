#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $filename = 'ejemplos/phonebook.txt';

my $lines = slurp $filename;
my @lines = split /\n/,$lines;
for @lines -> $line {
	print "L: $line\n";
}

my %phonebook = map {split ",", $_}, @lines;

print "Name:";
my $name = $*IN.get;
say %phonebook{$name};

