#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

warn "This is a warning";

say "Hello World";

# TODO: Parrot still gives a stack trace when calling die..
#die "This will kill the script";

#say "This will not show up";
