#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $filename = 'ejemplos/files/slurp_file.p6';

my $data = slurp $filename;
say $data.bytes;

my @rows = slurp $filename;
say @rows.elems;
