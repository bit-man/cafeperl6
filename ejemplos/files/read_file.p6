#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $filename = "ejemplos/files/read_file.p6";

if (my $fh = open $filename, :r) {
    while ! $fh.eof {
        my $line = $fh.get;
        say $line;
    }
} else {
    say "Could not open '$filename'";
}

