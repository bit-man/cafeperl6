#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my %user_a = ("fname", "Foo", "lname", "Bar");

my %user_b = (
    "fname" => "Foo", 
    "lname" => "Bar",
);

say %user_a{"fname"};
%user_a{"email"} = "foo@bar.com";
say %user_a{"email"};

say %user_b<lname>;

