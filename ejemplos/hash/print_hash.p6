#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my %user = (
    "fname" => "Foo", 
    "lname" => "Bar",
    "email" => "foo@bar.com",
);

for %user.keys.sort -> $key {
    say "$key  {%user{$key}}";
}

