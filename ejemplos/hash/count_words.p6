#!/usr/bin/perl6
##
## Copyright (C) 2009  Gabor Szabo  (http://szabgab.com/)
## Licensed under GNU GPL v2 
## http://www.gnu.org/licenses/gpl-2.0.html
##


use v6;

my $filename = 'ejemplos/words.txt';

my %counter;

my $fh = open($filename, :r) // die "Could not open '$filename'";
while !$fh.eof  {
    my $line = $fh.get;
    my @words = split /\s+/, $line;
    for @words -> $word {
        %counter{$word}++;
    }
}

for %counter.keys.sort -> $word {
    say "$word {%counter{$word}}";
}
