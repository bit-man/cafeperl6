## $Id$

=head1 NAME

src/builtins/dir.pir - Perl6 builtins for directory I/O

=head1 Functions

=over 4

=cut

.namespace []

=item opendir

Opens the directory passed as parameter

=cut

.sub 'myOpendir'
    .param string filename
    $P0 = readdir (filename)
    $S0 = read $P0, 10
    close $P0
    say $S0
    # Create IO object and set handle.
    .local pmc obj
    obj = get_hll_global 'IO'
    .return(obj)
.end

=back

=cut

# Local Variables:
#   mode: pir
#   fill-column: 100
# End:
# vim: expandtab shiftwidth=4 ft=pir:
